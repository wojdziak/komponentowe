import ipaddress
from math import ceil, log2


class Network(ipaddress.IPv4Network):

    def __init__(self,name,network):
        try:
            super().__init__(network)
        except ipaddress.AddressValueError:
            # print("Zły adres ip")
            raise ipaddress.AddressValueError
            
        except ipaddress.NetmaskValueError:
            # print("Zła maska")
            raise ipaddress.NetmaskValueError
            
        else:
            self.name = name            
            self.hostNumbers = self.num_addresses-2
            self.ipRange = f"{self.network_address + 1} - {self.network_address + self.hostNumbers}"      

    @classmethod
    def createSubnet(cls,name, numberOfHostsNeeded, startingIP):        
        ip = ipaddress.ip_address(startingIP)
        mask = 32 - ceil(log2(numberOfHostsNeeded + 2))        
        try:
            network = ipaddress.ip_network((ip,mask))
        except ValueError:            
            return None
        else:
            return Network(name,network)

    @classmethod
    def createSubnets(cls, data, startingIP):
        if not data:
            return None
        data.sort(reverse=True, key = lambda x : x[1]) #data format (name, number of hosts)
        subnets = []
        subnets.append(cls.createSubnet(data[0][0],data[0][1], startingIP))
        for i in range(1,len(data)):
            subnets.append(cls.createSubnet(data[i][0],data[i][1], subnets[i-1].broadcast_address + 1))
        return subnets


# net1 = Network("testowa", "192.168.0.0/20")
# print(net1.ipRange)

# data = [("s1", 28),
#         ("s2", 251),
#         ("s3", 233),
#         ("s4", 1024)]

# nets = net1.createSubnets(data, net1.network_address)
# for net in nets:
#     print(net.name, net.ipRange, net.hostNumbers)

