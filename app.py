from tkinter import *
from tkinter import filedialog, messagebox
import sys
import os


from Network import *


class NegativeNumberError(Exception):
    pass
class BiggerValueError(Exception):
    pass

def restart():    
    python = sys.executable
    os.execl(python, python, * sys.argv)

subnetsInfoLabels = []
subnetsNamesFields = []
subnetsSizeFields = []

def createSubnetsFields():
    for infoLabels, namesFields, sizeFields in zip(reversed(subnetsInfoLabels), reversed(subnetsNamesFields), reversed(subnetsSizeFields)):
        infoLabels.destroy()
        namesFields.destroy()
        sizeFields.destroy()

    subnetsInfoLabels.clear()
    subnetsNamesFields.clear()
    subnetsSizeFields.clear()

    for i in range(int(numberOfSubnets.get())):
        subnetInfoLabel = Label(text = "Podsieć " + str(i+1), fg='#e1f4f2')
        subnetInfoLabel.grid(row=i+3,column=0, pady=(10))
        subnetsInfoLabels.append(subnetInfoLabel)
        subnetNameField = Entry(background="#eae7d7")
        subnetNameField.grid(row=i+3,column=1, pady=(10))
        subnetsNamesFields.append(subnetNameField)
        subnetSizeField = Entry(background="#eae7d7")
        subnetSizeField.grid(row=i+3,column=2, pady=(10))
        subnetsSizeFields.append(subnetSizeField)

def changeCalcualteBtnPosition():
    calculateBtn.grid(row= int(numberOfSubnets.get()) + 3,column=1)

def changeNumberOfSubnets():
    try:
        number = int(subnetsNumberField.get())
        if number < 2 : raise NegativeNumberError
        if number > 8 : raise BiggerValueError
        numberOfSubnets.set(number)
    except ValueError:
        messagebox.showerror("Error", "Podaj liczbę")
    except NegativeNumberError:
        messagebox.showerror("Error", "Podaj liczbę większą od 1")
    except BiggerValueError:
        messagebox.showerror("Error", "Maksymalnie 8 podsieci")
    else:
        createSubnetsFields()
        changeCalcualteBtnPosition()


def takeData():
    data = []
    for namesFields, sizeFields in zip(subnetsNamesFields, subnetsSizeFields):        
        try:
            tmp = [str(namesFields.get()), int(sizeFields.get())]
        except ValueError:
            namesFields.configure({"background": "red"})
            sizeFields.configure({"background": "red"})
            messagebox.showerror("Error", "Uzupełnij pola")
            return None
        else:   
            namesFields.configure({"background": "white"})
            sizeFields.configure({"background": "white"})     
            data.append(tmp)

    subnetsNamesFields.clear()
    subnetsSizeFields.clear()
    return data

def calculateSubnets():
    try:
        net = Network("Main network", majorNetworkField.get())
    except ValueError:
        messagebox.showerror("Error", "Podaj adres sieci w formacie a.b.c.d/maska \nNp. 192.168.1.0/25")
    else:
        data = takeData()
        if data:
            try:                
                subnets = Network.createSubnets(data, net.network_address)
                showResult(subnets) 
                               
            except (AttributeError, ValueError):
                messagebox.showerror("Error", "Nie można dokonać podziału!")
    
def saveToFile(subnets):    
    f = filedialog.asksaveasfile(mode='w', defaultextension=".txt")
    if f is None:
        return
    text = ""
    for subnet in subnets:
        text += "Nazwa podsieci: " + subnet.name
        text += "\tZarezerwowana ilość hostów: " + str(subnet.num_addresses -2)
        text += "\tAdres podsieci: " + str(subnet.network_address)
        text += "\tMaska: " + str(subnet.netmask)
        text += "\tIpRange: " + str(subnet.ipRange)
        text += "\tBroadcast: " + str(subnet.broadcast_address)
        text += "\n" 
    
    f.write(text)
    f.close()
    
def showResult(subnets): 
    
    result = Toplevel()
    result.title("Podział sieci")
    result.geometry("800x500")

    subnetNameLabel = Label(master=result, text="Nazwa podsieci")
    subnetNameLabel.grid(row=0,column=0,padx=(10, 10), pady=(30, 20))    

    subnetAllocatedSizeLabel = Label(master=result, text="Zarezerwowana ilość hostów")
    subnetAllocatedSizeLabel.grid(row=0,column=1,padx=(10, 10), pady=(30, 20))

    subnetNetworkAddressLabel = Label(master=result, text="Adres podsieci")
    subnetNetworkAddressLabel.grid(row=0,column=2,padx=(10, 10), pady=(30, 20))

    subnetMaskLabel = Label(master=result, text="Maska")
    subnetMaskLabel.grid(row=0,column=3,padx=(10, 10), pady=(30, 20))

    subnetIpRangeLabel = Label(master=result, text="IP Range")
    subnetIpRangeLabel.grid(row=0,column=4,padx=(10, 10), pady=(30, 20))

    subnetBroadcastLabel = Label(master=result, text="Broadcast")
    subnetBroadcastLabel.grid(row=0,column=5,padx=(10, 10), pady=(30, 20))

    generateResults(result, subnets)


    numberOfSubnets.set(2)

    saveBtn = Button(result, text="Zapisz do pliku", background='#756d47')
    saveBtn["command"] = lambda : saveToFile(subnets)
    saveBtn.grid(row=10,column=0,padx=(10, 10), pady=(30, 20))


    resBtn = Button(result, text="Nowa siec", background='#756d47')
    resBtn["command"] =restart
    resBtn.grid(row=10,column=1,padx=(10, 10), pady=(30, 20))

    exitBtn = Button(result, text="Zamknij", background='#756d47')
    exitBtn["command"] = window.destroy
    exitBtn.grid(row=10,column=2,padx=(10, 10), pady=(30, 20))


subnetNameList = []
subnetAllocatedSizeList = []
subnetNetworkAddressList = []
subnetMaskList = []
subnetIpRangeList = []
subnetBroadcastList = []

def generateResults(masterFrame,subnets):     
    result = masterFrame
    i = 1
    for subnet in subnets:
        subnetName = Label(master=result, text=subnet.name)
        subnetName.grid(row=i,column=0)
        subnetNameList.append(subnetName)

        subnetAllocatedSize = Label(master=result, text=str(subnet.num_addresses-2))
        subnetAllocatedSize.grid(row=i,column=1)
        subnetAllocatedSizeList.append(subnetAllocatedSize)

        subnetNetworkAddress = Label(master=result, text=str(subnet.network_address))
        subnetNetworkAddress.grid(row=i,column=2)
        subnetNetworkAddressList.append(subnetNetworkAddress)

        subnetMask = Label(master=result, text=str(subnet.netmask))
        subnetMask.grid(row=i,column=3)
        subnetMaskList.append(subnetMask)

        subnetIpRange = Label(master=result, text=str(subnet.ipRange))
        subnetIpRange.grid(row=i,column=4)
        subnetIpRangeList.append(subnetIpRange)

        subnetBroadcast = Label(master=result, text=str(subnet.broadcast_address))
        subnetBroadcast.grid(row=i,column=5)
        subnetBroadcastList.append(subnetBroadcast)

        i += 1
    

window = Tk()
window.title("VLSM calculator")
window.geometry("800x600")
window.resizable(False, False)
window.option_add("*Font", 'Arial')
window.configure(bg="#42413b")
window.option_add("*Background", "#42413b")

mainFrame = Frame(window)
mainFrame.grid()

majorNetworkLabel=Label(text="Podaj adres sieci", fg='#e1f4f2')
majorNetworkLabel.grid(row=0,column=0, padx=(10, 10), pady=(30, 20))

majorNetworkField=Entry(background="#eae7d7")
majorNetworkField.grid(row=0,column=1, padx=(10, 10), pady=(30, 20))

c1 = Canvas(window, height=2, width=780, bg='#33a095')
c1.grid(row=1, columnspan=4, pady=(10, 10), padx=(10))

subnetsNumberLabel=Label(text="Ilość podsieci", fg='#e1f4f2')
subnetsNumberLabel.grid(row=2,column=0, padx=(10, 10))

numberOfSubnets = StringVar(value="2")
subnetsNumberErrorLabel = Label(textvariable = numberOfSubnets, fg='#e1f4f2')
subnetsNumberErrorLabel.grid(row=2,column=1, padx=(10, 10))

subnetsNumberField=Entry(background="#eae7d7")
subnetsNumberField.grid(row=2,column=2, padx=(10, 10))

subnetsNumberBtn = Button(text="Zmien", background='#756d47')
subnetsNumberBtn["command"] = changeNumberOfSubnets
subnetsNumberBtn.grid(row=2,column=3, padx=(10, 10))

subnetsNameLabel = Label(text = "Nazwa", fg='#e1f4f2')
subnetsNameLabel.grid(row=3,column=1, padx=(10, 10), pady=(10, 10))

subnetsNameLabel = Label(text = "Nazwa", fg='#e1f4f2')
subnetsNameLabel.grid(row=3,column=1, padx=(10, 10), pady=(10, 10))

subnetsSizeLabel = Label(text = "Ilość hostów", fg='#e1f4f2')
subnetsSizeLabel.grid(row=3,column=2, padx=(10, 10), pady=(10, 10))

createSubnetsFields()

calculateBtn = Button(text="Podziel", background='#756d47')
calculateBtn["command"] = calculateSubnets
calculateBtn.grid(row= int(numberOfSubnets.get()) + 3,column=2, pady=(10, 10))

if __name__ == "__main__":
    window.mainloop()



